$( document ).ready(function() {
  
  // Age verification form.
  $("#demoForm").submit(function(e){
    e.preventDefault();
    var day = $("#birthDay").val();
    var month = $("#birthMonth").val();
    var year = $("#birthYear").val();
    var age = $("#requiredAge").val();
    if (day == "" || month == "" || year == "") {
      alert("You must enter all fields.");
      return false;
    }
    var mydate = new Date();
    mydate.setFullYear(year, month-1, day);

    var currdate = new Date();
    currdate.setFullYear(currdate.getFullYear() - age);
    if ((currdate - mydate ) < 0){
      alert("Sorry, only persons over the age of " + 21 + " may enter this site");
      return false;
    }
    alert("Success! Submitting Form…");
    return true;
  });
  
  
});
